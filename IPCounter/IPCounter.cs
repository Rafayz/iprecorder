﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IPCounter
{
    public static class IPCounter
    {
        public static Dictionary<string, int> IPRecords = new Dictionary<string, int>();

        public static int RecordIP(string ipAddress)
        {
            try
            {

                int value;
                if (IPRecords.TryGetValue(ipAddress, out value)) // Returns true.
                {
                    IPRecords[ipAddress] = ++value;
                }
                else
                {
                    value = 1;
                    IPRecords.Add(ipAddress, value);
                }


                return value;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
