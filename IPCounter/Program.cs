﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IPCounter
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("********** IP RECORDER **********");
                string shallContinue = "y";
                do
                {
                    Console.WriteLine("\n\nEnter an IP Address :");
                    string ipValue = Console.ReadLine();

                    int ipCount = IPCounter.RecordIP(ipValue);

                    Console.WriteLine("\nThis IP has been recorded {0} times", ipCount.ToString());
                    Console.WriteLine("**********************************");

                   Console.WriteLine("Do you want to continue (Y/N):");
                   shallContinue = Console.ReadLine();


                } while (shallContinue.ToLower() == "y");

            }
            catch (Exception ex)
            {
                Console.WriteLine("EXCEPTION: " + ex.Message);
            }
        }
    }
}
